export const environment = {
  production: false,
  PokeApiBaseUrl: 'https://pokeapi.co/api/v2/pokemon',
  trainerAPI: 'http://localhost:3000',
};
