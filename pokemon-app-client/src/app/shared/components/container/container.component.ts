import {Component} from '@angular/core';

@Component({
  selector: 'app-container-component',
  templateUrl: './container.component.html',
  // Inline styles in Angular
  styles: [
    `.containercomp {
      max-width: 75em; 
      width: 100%;
      margin: 0 auto;
      padding: 0 1em;
    }`
  ]
})
export class AppContainerComponent {
}
