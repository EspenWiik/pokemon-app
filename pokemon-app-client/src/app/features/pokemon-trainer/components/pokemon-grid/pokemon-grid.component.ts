import { Component, Input, EventEmitter, Output } from "@angular/core";
import { Router } from "@angular/router";


@Component({
    selector: 'app-pokemon-grid',
    templateUrl: '/pokemon-grid.component.html',
    styles: [
		'.pokemon {background-color: #F87575; box-shadow: 3px 5px;}'
	]

})

export class PokemonGridComponent {
    constructor(
		private readonly router: Router
	) { }

    @Input() pokemon;

    pokemonClicked( id:string): void {
        this.router.navigate([`./pokemon/${id}`])
    }
}