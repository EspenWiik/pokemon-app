import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs';
import { Pokemon } from '../../pokemon/models/pokemon.model'
import { environment } from "src/environments/environment";
//import { PokemonDetails } from '../models/pokemon-details.model'
import { map } from 'rxjs/operators';
import { getStorage } from 'src/app/utils/storage.utils';
const { trainerAPI } = environment;

@Injectable({
	providedIn: 'root'
})

export class PokemonTrainerService {

    private readonly _pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);


    constructor(private readonly http: HttpClient) { }

    public pokemons$(): Observable<Pokemon[]> {

        return this._pokemons$.asObservable();
    }


    public fetchPokemon(): void {
        const trainerName = getStorage<any>('pk-tr');
        console.log(`${trainerAPI}/pokemon?trainerName=${trainerName.name}`)
        this.http.get(`${trainerAPI}/pokemon?trainerName=${trainerName.name}`)
        .subscribe(
            (pokemon: Pokemon[]) => {
                this._pokemons$.next(pokemon)
					console.log(pokemon);
	
				},
				(error) => {
					console.log(error);
				}
        )
    }




}