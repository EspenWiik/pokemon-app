import { Component } from "@angular/core";
import { PokemonTrainerService} from '../servies/pokemon-trainer.service'
import { Observable } from "rxjs";
import { Pokemon } from '../../pokemon/models/pokemon.model'

@Component({
	selector: 'app-pokemon-trainer.container',
	templateUrl: './pokemon-trainer.container.html',
	styleUrls: ['./pokemon-trainer.container.css']

})

export class PokemonTrainerContainer { 

	constructor(
		private readonly trainerService: PokemonTrainerService
	) { }

	ngOnInit(): void {
		this.trainerService.fetchPokemon();

	}

	get pokemons$(): Observable<Pokemon[]> {
		return this.trainerService.pokemons$();
	}

}