export interface PokeApiResponse {
	count: number;
	next: string;
	previous: any;
	results: any;
}

