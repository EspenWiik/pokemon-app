import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { PokeApiResponse } from '../models/api-response.model';
import { Pokemon } from '../models/pokemon.model';
import { map } from 'rxjs/operators';

const { PokeApiBaseUrl } = environment;

@Injectable({
    providedIn: 'root'
})
export class PokemonService {

    // Observable!
    private readonly _pokemons$: BehaviorSubject<Pokemon[]> = new BehaviorSubject([]);


    constructor(private readonly http: HttpClient) { }

    public pokemons$(): Observable<Pokemon[]> {

        return this._pokemons$.asObservable();
    }

    public fetchPokemon(): void {
        this.http.get<PokeApiResponse>(`${PokeApiBaseUrl}`)
            .pipe(
                map((response: PokeApiResponse) => response.results)
            )
            .subscribe(
                (pokemons: Pokemon[]) => {

                    for (let index = 0; index < pokemons.length; index++) {
                        const url = pokemons[index].url;

                        pokemons[index].imgUrl = this.getIdAndImage(url).url
                        pokemons[index].id = this.getIdAndImage(url).id
                    }

                    this._pokemons$.next(pokemons);

                    console.log(this._pokemons$);

                    console.log(pokemons);
                },
                (error) => {
                    console.log('MoviesService.fetchMovies().error', error);
                }
            );
    }

    public getIdAndImage(url: string): any {
        const id = url.split('/').filter(Boolean).pop();

        return {
            id,
            url:
                `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
        };
    }





}