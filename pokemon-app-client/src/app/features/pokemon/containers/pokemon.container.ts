import { Component } from "@angular/core";
import { Router } from "@angular/router";
import { Observable } from "rxjs";
import { Pokemon } from "src/app/features/pokemon/models/pokemon.model";
import { PokemonService } from "src/app/features/pokemon/services/pokemon.service";


@Component({
	selector: 'app-pokemon-container',
	templateUrl: './pokemon.container.html',
	styleUrls: ['./pokemon.container.css' ]
})
export class PokemonContainer {

	error: string = '';

	constructor(
		private readonly pokemonService: PokemonService,
		private readonly router: Router
	) { }


	// Component has loaded.
	ngOnInit(): void {
		this.pokemonService.fetchPokemon();

	}

	get pokemons$(): Observable<Pokemon[]> {
		return this.pokemonService.pokemons$();
	}

	onPokemonClicked(name: string): void {

		this.router.navigate([`./pokemon/${name}`])
	}




	// // Event Handler for Output from MovieComponent
	// handleMovieClicked(movieId: number): void {
	// 	const movie = this.movies.find(m => m.id === movieId);
	// 	alert(`You clicked on ${movie.title}`);
	// }

}