import { Component, Input, EventEmitter, Output } from "@angular/core";


@Component({
	selector: 'app-pokemon',
	templateUrl: './pokemon.component.html',
	styles: [
		'.article {background-color: #F87575; box-shadow: 3px 5px;}'
	]
})
export class PokemonComponent {

	@Input() pokemon;
	@Output() clicked: EventEmitter<string> = new EventEmitter();




	constructor() { }




	onPokemonClicked(): void {
		this.clicked.emit(this.pokemon.id)


	}





}



