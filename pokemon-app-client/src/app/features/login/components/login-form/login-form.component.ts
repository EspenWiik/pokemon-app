import { Component, EventEmitter, OnInit, Output } from "@angular/core";
import { FormGroup, FormControl, Validators, AbstractControl } from "@angular/forms";
import { finalize } from "rxjs/operators";
import { getStorage } from "../../../../utils/storage.utils"
import { LoginService } from "../../services/login.service";

@Component({
    selector: 'app-login-form',
    templateUrl: './login-form.component.html',
    styles: [
        `
      label {
        display: block;
        margin-bottom: 1em
      }
      input {
        display: block;
      }
    `,
    ],
})
export class LoginFormComponent implements OnInit{

    @Output() success: EventEmitter<void> = new EventEmitter();
  
    loginForm: FormGroup = new FormGroup({
        trainerName: new FormControl('',[
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(20)
        ]),
    });

    constructor(private readonly loginService: LoginService) {      
    }

    ngOnInit(): void {
        const existingTrainer = getStorage<any>('pk-tr');
        if (existingTrainer !== null) {
            this.success.emit();
        }
    }

    get trainerName(): AbstractControl {
        return this.loginForm.get('trainerName')
    }

    get loading(): boolean {
        return this.loginService.loading;
    }

    get error(): string {
        return this.loginService.error;
    }

    onStartClick() {
        console.log('onStartClick '+this.loginForm.value)
        const {trainerName} = this.loginForm.value;
        this.loginService.login(trainerName)
            .subscribe(                
                this.handleSuccessfulLogin.bind(this),
                this.handleErrorLogin.bind(this)
            );
    }
/*
    onRegisterClick() {
        const {trainerName} = this.loginForm.value;
        this.loginService.register(trainerName)
            .subscribe(                
                this.success.emit();
            );
    }*/

    handleSuccessfulLogin(trainer): void {
        this.success.emit();
    }

    handleErrorLogin(error): void {
        if (this.loginService.errorCount >= 3) {
            
        }

    }
}