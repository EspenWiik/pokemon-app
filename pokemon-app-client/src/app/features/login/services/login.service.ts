import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable, of, throwError } from "rxjs";
import { finalize, map, mergeMap, tap } from "rxjs/operators";
import { StorageKeys } from "src/app/enums/storage-keys.enum";
import { SessionService } from "src/app/shared/services/session.service";
import { environment } from "src/environments/environment";
import { setStorage } from "../../../utils/storage.utils"
const { trainerAPI } = environment;

@Injectable({
    providedIn: 'root'

})

export class LoginService {

    public loading: boolean = false;
    public error: string = '';
    public errorCount: number = 0;

    constructor(private readonly http: HttpClient){}
 
    private setTrainer(trainer: any) {
        setStorage(StorageKeys.TRAINER, trainer);
    }

   

    login(trainerName: string): Observable<any> {
        this.loading = true;

         const login$ = this.http.get(`${trainerAPI}/trainers?name=${trainerName}`);
         const register$ = this.http.post(`${trainerAPI}/trainers`, {
             name: trainerName 
         });        

        return login$.pipe(
                mergeMap((loginResponse: any[]) => {
                    const trainer = loginResponse.pop();
                    if(!trainer) {
                        console.log("throwing ", register$)
                        return register$;
                    } else {

                        return of(trainer);
                    }
                }),
                tap((trainer) => {
                    this.setTrainer(trainer);
                }),               
                finalize(() => {
                    this.loading = false;
                })
            )
    }
}