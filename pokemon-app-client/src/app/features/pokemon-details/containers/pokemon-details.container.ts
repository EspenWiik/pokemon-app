import { Component } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Observable } from "rxjs";
import { PokemonDetailsService } from "src/app/features/pokemon-details/services/pokemon-details.service";
import { getStorage } from "src/app/utils/storage.utils";
import { environment } from "src/environments/environment";
import { PokemonDetails } from '../models/pokemon-details.model'
const { trainerAPI } = environment;

@Component({
	selector: 'app-pokemon-details-container',
	templateUrl: './pokemon-details.container.html',
	styleUrls: ['./pokemon-details.container.css']

})

export class PokemonDetailsContainer {

	pokemonName: string = '';

	constructor(private route: ActivatedRoute,
		private pokemonDetailService: PokemonDetailsService) {
		// ActivatedRoute == Info about the current route.
		this.pokemonName = this.route.snapshot.paramMap.get('name');
		
	}

	ngOnInit() {

		
		this.pokemonDetailService.fetchPokemonDetails(this.pokemonName)

	}

	get pokemonDetails$(): Observable<PokemonDetails[]> {
		return this.pokemonDetailService.pokemonDetails$();
	}
	
	

	onCatchClicked(name: string, imgUrl: string): any {
		
		let id = this.pokemonName;
		const trainer = getStorage<any>('pk-tr');
		const trainerName = trainer.name
		return fetch(`${trainerAPI}/pokemon`, { 
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({ trainerName, id, name, imgUrl  })
		})
			.then(r => r.json()) 

	}

}