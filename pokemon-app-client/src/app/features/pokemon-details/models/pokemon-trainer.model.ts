import { PokemonDetails } from "./pokemon-details.model";


export interface Trainer {
    id?: number;
    name: string;
    pokemon: PokemonDetails[];
}