import { PokemonType } from './pokemon-details-type.model'
import { PokemonAbility } from './pokemon-details-abilities.model'
import { PokemonMove } from './pokemon-details-moves.model'

export interface PokemonDetails {
    imgUrl: string
    base_experience: number,
    height: number,
    weight: number,
    name: string,
    id: number,
    types: PokemonType[],
    abilities: PokemonAbility[],
    moves: PokemonMove[],
    
    stats: [
        {
            base_stat: number
        }
    ]

    
}


