export interface PokemonAbility {
    ability: {
        name: string
    }
    slot: number
}