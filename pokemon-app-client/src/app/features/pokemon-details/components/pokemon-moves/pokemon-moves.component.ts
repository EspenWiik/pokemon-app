import { Component, Input} from "@angular/core";
import { PokemonMove } from "../../models/pokemon-details-moves.model";

@Component({
	selector: 'app-move',
	templateUrl: './pokemon-moves.component.html',
})

export class PokemonMoveComponent {

    @Input() move: PokemonMove;
    
    

    constructor() { }
}