import { Component, Input} from "@angular/core";
import { PokemonAbility } from "../../models/pokemon-details-abilities.model";

@Component({
	selector: 'app-ability',
	templateUrl: './pokemon-abilities.component.html',
})

export class PokemonAbilityComponent {

    @Input() ability: PokemonAbility;
    
    

    constructor() { }
}