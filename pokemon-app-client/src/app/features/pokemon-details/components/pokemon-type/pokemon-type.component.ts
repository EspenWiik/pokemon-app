import { Component, Input} from "@angular/core";
import { PokemonType } from "../../models/pokemon-details-type.model";

@Component({
	selector: 'app-type',
	templateUrl: './pokemon-type.component.html',
})

export class PokemonTypeComponent {

    @Input() type: PokemonType;

    constructor() { }
}