import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core'
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from "src/environments/environment";
import { PokemonDetails } from '../models/pokemon-details.model'
import { map } from 'rxjs/operators';
import { PokemonTrainerContainer } from '../../pokemon-trainer/containers/pokemon-trainer.container';

const { PokeApiBaseUrl } = environment;

@Injectable({
	providedIn: 'root'
})
export class PokemonDetailsService {


	private readonly _pokemonDetails$: BehaviorSubject<PokemonDetails[]> = new BehaviorSubject([]);


	constructor(private readonly http: HttpClient) { }

	public pokemonDetails$(): Observable<PokemonDetails[]> {
		return this._pokemonDetails$.asObservable();
	}

	public fetchPokemonDetails(id): void {
		this.http.get(`${PokeApiBaseUrl}/${id}`)
		.pipe(
			map((pokemonDetails: PokemonDetails) => ({
				...pokemonDetails, 
				...this.getImg(id),	
			}))
		)
// return the new object, with all the original values + imgUrl property
		.subscribe(
				(pokemonDetails: PokemonDetails[]) => {
					this._pokemonDetails$.next(pokemonDetails)
					console.log(pokemonDetails);
	
				},
				(error) => {
					console.log(error);
				}
			);			
	}

	public getImg(id: string): any {
		return {
			imgUrl: `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`
		}
	}

	
		






}