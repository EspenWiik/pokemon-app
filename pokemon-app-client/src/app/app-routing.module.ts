import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PokemonContainer } from "./features/pokemon/containers/pokemon.container";
import { PokemonDetailsContainer } from "./features/pokemon-details/containers/pokemon-details.container"
import { LoginContainer } from './features/login/containers/login.container';
import { SessionGuard } from './guards/session.guard';
import { PokemonTrainerContainer } from './features/pokemon-trainer/containers/pokemon-trainer.container';

const routes: Routes = [
	{
		path: '',
		pathMatch: 'full',
		redirectTo: '/login'
	},
	
	{
		path: 'login',
		component: LoginContainer
	},

	{
		path: 'pokemon',
		component: PokemonContainer,
		canActivate: [SessionGuard]
	},
	{
		path:'pokemon/:name',
		component: PokemonDetailsContainer,
		canActivate: [SessionGuard]
	},
	{
		path:'trainer',
		component: PokemonTrainerContainer,
		canActivate: [SessionGuard]
	}
];

@NgModule({
	imports: [ RouterModule.forRoot( routes ) ],
	exports: [ RouterModule ]
})
export class AppRoutingModule { }
