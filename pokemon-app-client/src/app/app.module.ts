import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PokemonComponent } from './features/pokemon/components/pokemon.component';
import { PokemonContainer } from './features/pokemon/containers/pokemon.container';
import { PokemonDetailsContainer } from './features/pokemon-details/containers/pokemon-details.container'
import { PokemonTypeComponent } from './features/pokemon-details/components/pokemon-type/pokemon-type.component'
import { PokemonAbilityComponent } from './features/pokemon-details/components/pokemon-abilites/pokemon-abilities.component'
import { RouterModule } from '@angular/router';
import { PokemonMoveComponent } from './features/pokemon-details/components/pokemon-moves/pokemon-moves.component'
import { AppContainerComponent } from './shared/components/container/container.component';
import { ReactiveFormsModule } from '@angular/forms';
import { LoginContainer } from './features/login/containers/login.container';
import { BaseButtonComponent } from './shared/components/base-button/base-button.component';
import { LoginFormComponent } from './features/login/components/login-form/login-form.component';
import { NavbarComponent } from './shared/components/navbar/navbar.component';
import { PokemonTrainerContainer } from './features/pokemon-trainer/containers/pokemon-trainer.container';
import { PokemonGridComponent } from './features/pokemon-trainer/components/pokemon-grid/pokemon-grid.component';

@NgModule({
	declarations: [
		AppComponent,
		AppContainerComponent,
		NavbarComponent,
		LoginContainer,
		LoginFormComponent,
		PokemonContainer,
		PokemonComponent,
		PokemonDetailsContainer,
		PokemonTypeComponent,
		PokemonAbilityComponent,
		PokemonMoveComponent,
		BaseButtonComponent,
		PokemonTrainerContainer,
		PokemonGridComponent

	],
	imports: [
		BrowserModule,
		HttpClientModule,
		AppRoutingModule,
		ReactiveFormsModule,
		RouterModule
		
	],
	providers: [],
	bootstrap: [AppComponent]
})
export class AppModule {
}
